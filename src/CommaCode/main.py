def convertListToString(input):
    if input is None:
        return ''

    result = ''
    for i in input:
        if i == input[len(input) - 1]:
            add = ', and ' + i
        else:
            add = ', ' + i
        result += add
    return result


spam = ['apples', 'bananas', 'tofu', 'cats']
newlist = convertListToString(spam)

print(newlist)
