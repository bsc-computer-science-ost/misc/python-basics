class ParentClass:
    def printHello(self):
        print('Hello, world')

class ChildClass(ParentClass):
    def someNewMethod(self):
        print('ParentClass object don`t has this Mehtod')

class GrandchildClass(ChildClass):
    def anotherNewMethod(self):
        print('Only GrandchildClass objects have this method.')
