class Airplane:
    def flyInTheAir(self):
        print('flying')

class Ship:
    def floatOnWater(self):
        print('floating')

class FlyingBoat(Airplane, Ship):
    pass