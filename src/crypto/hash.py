import hashlib

m = hashlib.sha256()
m.update(b"Hello World")
m.digest()

print(m.hexdigest())