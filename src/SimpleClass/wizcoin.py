class WizCoin:
    def __init__(self, galleons, sickles, knuts):
        """Create a new WizCoin objec with galleons, sickels and knuts
        """
        self.galleons = galleons
        self.sickles = sickles
        self.knuts = knuts
        # NOTE: the __init()__ methods never have a return statement

    def value(self):
        """The value (in knuts) of all the coins in the WizCoin object"""
        return (self.galleons * 17 * 29) + (self.sickles * 29) + self.knuts

    def weightInGrams(self):
        """Returns the weigth of the coins in grams"""
        return (self.galleons * 31.103) + (self.sickles * 11.34) + (self.knuts * 5.0)