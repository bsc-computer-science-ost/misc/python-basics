import sys


def collatz(number):
    if number is None: return

    if 0 == number % 2:
        print(number / 2)
        return number / 2
    else:
        print((3 * number) + 1)
        return (3 * number) + 1


print('Welcome to the Collatz Sequence')
print('Please enter an Integer value')

try:
    input = input()
    current_value = int(input)
except ValueError:
    print('Please make sure you enter an int')
    sys.exit()

while current_value != 1:
    current_value = collatz(current_value)
