import random

numberOfStreaks = 0

for experimentNumber in range(10000000):
    result = []
    allHeads = True

    for i in range(0, 6):
        result.append(random.randint(0, 1))

    for i in result:
        if i == 0:
            allHeads = False
            break

    if allHeads:
        numberOfStreaks += 1

print(numberOfStreaks)
