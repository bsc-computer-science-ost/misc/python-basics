\section{Lists}
A list is a value that contains multiple values in an ordered sequence. The term list value refers to the list itself (which is a value that can be stored in a variable or passed to a function like any other value), not the values inside the list value.  A list value looks like this: \lstinline|['cat', 'bat', 'rat', 'elephant']|. Just as string values are typed with quote characters to mark where the string begins and ends, a list begins with an opening square bracket and ends with a closing square bracket, \lstinline|[]|.

Say you have the list \lstinline|['cat', 'bat', 'rat', 'elephant']| stored in a variable named spam. The Python code spam[0] would evaluate to 'cat', and spam[1] would evaluate to 'bat', and so on. The integer inside the square brackets that follows the list is called an index. The first value in the list is at index 0, the second value is at index 1, the third value is at index 2, and so on. 

\begin{figure}[h!]
  \center
  \includegraphics[width=0.5\textwidth]{list}
  \caption{Python List}
\end{figure}

Lists can also contain other list values. The values in these lists of lists can be accessed using multiple indexes. The first index dictates which list value to use, and the second indicates the value within the list value.

\begin{lstlisting}
spam = ['cat', 'bat', 'rat', 'elephant'] 
spam[0] # evaluates to 'cat'
spam[1] = 'aardvark'  # changes the value
del spam[1] # deletes the entry 

spam = [['cat', 'bat'], [10, 20, 30, 40, 50]] 
spam[0][1] # evaluates to ['cat', 'bat']
\end{lstlisting}

\paragraph{Negative Indexes}
While indexes start at 0 and go up, you can also use negative integers for the index. The integer value -1 refers to the last index in a list, the value -2 refers to the second-to-last index in a list, and so on. 

\paragraph{Slices}
Just as an index can get a single value from a list, a slice can get several values from a list, in the form of a new list. A slice is typed between square brackets, like an index, but it has two integers separated by a colon. Notice the difference between indexes and slices. In a slice, the first integer is the index where the slice starts. The sec- ond integer is the index where the slice ends. A slice goes up to, but will not include, the value at the second index.

\begin{itemize}
	\item spam[2] is a list with an index
	\item spam[1:4] is a list with a slice
\end{itemize}

\paragraph{Lists Length}
The len() function will return the number of values that are in a list value passed to it, just like it can count the number of characters in a string value. 

\begin{lstlisting}
spam ['cat', 'dog', 'moose']
len(spam) # evaluates to 3
\end{lstlisting}

\paragraph{Using Loops with Lists}
Using range(len(supplies)) in the  shown for loop is handy because the code in the loop can access the index (as the variable i) and the value at that index (as supplies[i]). Best of all, range(len(supplies)) will iterate through all the indexes of supplies, no matter how many items it contains.

\begin{lstlisting}
supplies = ['pens', 'staplers', 'flamethrowers', 'binders'] 
for i in range(len(supplies)):
	 print('Index ' + str(i) + ' in supplies is: ' + supplies[i])
	 
# better code with enumerate
supplies = ['pens', 'staplers', 'flamethrowers', 'binders'] 
for index, item in enumerate(supplies): 
	 print('Index ' + str(index) + ' in supplies is: ' + item)
\end{lstlisting}

\subsection{In and not Operators}
You can determine whether a value is or isn’t in a list with the in and not in operators. Like other operators, in and not in are used in expressions and connect two values: a value to look for in a list and the list where it may be found. These expressions will evaluate to a Boolean value. 

\begin{lstlisting}
'howdy' in ['hello', 'hi', 'howdy', 'heyas'] # evaluates to True
spam = ['hello', 'hi', 'howdy', 'heyas'] 
'cat' in spam # evaluates to False
\end{lstlisting}

\paragraph{Tuple Unpacking}
The multiple assignment trick (technically called tuple unpacking) is a shortcut that lets you assign multiple variables with the values in a list in one line of code. The number of variables and the length of the list must be exactly equal, or Python will give you a ValueError.

\begin{lstlisting}
cat =['fat', 'gray', 'loud']
size, color, diposition = cat
\end{lstlisting}

\subsection{Methods}
A method is the same thing as a function, except it is “called on” a value. For example, if a list value were stored in spam, you would call the index() list method (which I’ll explain shortly) on that list like so: spam.index('hello'). The method part comes after the value, separated by a period. Each data type has its own set of methods. The list data type, for example, has several useful methods for finding, adding, removing, and otherwise manipulating values in a list.

\paragraph{Index Method}
List values have an index() method that can be passed a value, and if that value exists in the list, the index of the value is returned. If the value isn’t in the list, then Python produces a ValueError error.

\begin{lstlisting}
spam = ['hello', 'hi', 'howdy', 'heyas'] 
spam.index('hello') # return 0
spam.index('audi') # throws a value error
\end{lstlisting}

\paragraph{Append and Insert Methods}
To add new values to a list, use the append() and insert() methods. The previous append() method call adds the argument to the end of the list. The insert() method can insert a value at any index in the list. The first argument to insert() is the index for the new value, and the second argument is the new value to be inserted

\begin{lstlisting}
spam = ['cat', 'dog', 'bat'] 
spam.append('moose')
spam.insert(1, 'chicken')
\end{lstlisting}

\paragraph{Remove Method}
The remove() method is passed the value to be removed from the list it is called on. Attempting to delete a value that does not exist in the list will result in a ValueError error.

\begin{lstlisting}
spam = ['cat', 'bat', 'rat', 'elephant'] 
spam.remove('bat') 
spam.remove('chicken') # throws a value error
\end{lstlisting}

\paragraph{Sort Method}
Lists of number values or lists of strings can be sorted with the sort() method. You can also pass True for the reverse keyword argument to have sort() sort the values in reverse order.  You cannot sort lists that have both number values and string values in them, since Python doesn’t know how to compare these values. The sort() uses “ASCIIbetical order” rather than actual alphabetical order for sorting strings. This means uppercase letters come before lower- case letters. Therefore, the lowercase a is sorted so that it comes after the uppercase Z. If you need to sort the values in regular alphabetical order, pass str.lower for the key keyword argument in the sort() method call.

\begin{lstlisting}
spam = [2, 5, 3.14, 1, -7] 
spam.sort() 
spam.sort(reverse=True) 
spam.sort(key=str.lower)
\end{lstlisting}

\paragraph{Reverse Method}
If you need to quickly reverse the order of the items in a list, you can call the reverse() list method.

\begin{lstlisting}
spam.reverse()
\end{lstlisting}

\subsection{Sequence Data Types}
Lists aren’t the only data types that represent ordered sequences of values. For example, strings and lists are actually similar if you consider a string to be a “list” of single text characters. The Python sequence data types include lists, strings, range objects returned by range(), and tuples Many of the things you can do with lists can also be done with strings and other values of sequence types: index- ing; slicing; and using them with for loops, with len(), and with the in and not in operators.

\begin{lstlisting}
name = 'Zophie'
name[0] # Z
name[0:4] # Zoph
'Zo' in name # True
for i in name:
	print('***' + i + '***') 
\end{lstlisting}

\paragraph{Mutable and Immutable Data Types}
But lists and strings are different in an important way. A list value is a mutable data type: it can have values added, removed, or changed. However, a string is immutable : it cannot be changed. Trying to reassign a single character in a string results in a TypeError error. The proper way to “mutate” a string is to use slicing and concatenation to build a new string by copying from parts of the old string.

\begin{lstlisting}
name = 'Zophie a cat'
newName = name[0:7] 1 'the' + name[8:12]
\end{lstlisting}

Although a list value is mutable, the second line in the following code does not modify the list eggs. The list value in eggs isn’t being changed here; rather, an entirely new and different list value ([4, 5, 6]) is overwriting the old list value ([1, 2, 3]).

\begin{lstlisting}
eggs = [1, 2, 3]
eggs = [4, 5, 6]
\end{lstlisting}

Changing a value of a mutable data type (like what the del statement and append() method do in the previous example) changes the value in place, since the variable’s value is not replaced with a new list value. Mutable versus immutable types may seem like a meaningless distinction, but “Passing References” 

\subsection{Tuple}
The tuple data type is almost identical to the list data type, except in two ways. First, tuples are typed with parentheses, ( and ), instead of square brackets, [ and ]. But the main way that tuples are different from lists is that tuples, like strings, are immutable. Tuples cannot have their values modified, appended, or removed. 

If you have only one value in your tuple, you can indicate this by plac- ing a trailing comma after the value inside the parentheses. Otherwise, Python will think you’ve just typed a value inside regular parentheses. The comma is what lets Python know this is a tuple value. (Unlike some other programming languages, it’s fine to have a trailing comma after the last item in a list or tuple in Python.)

\begin{lstlisting}
type(('hello', )) # class tuple
type(('hello')) # class str
\end{lstlisting}

You can use tuples to convey to anyone reading your code that you don’t intend for that sequence of values to change. If you need an ordered sequence of values that never changes, use a tuple. A second benefit of using tuples instead of lists is that, because they are immutable and their contents don’t change, Python can implement some optimizations that make code using tuples slightly faster than code using lists.

\paragraph{Converting Types}
Just like how str(42) will return '42', the string representation of the integer 42, the functions list() and tuple() will return list and tuple versions of the values passed to them.

\begin{lstlisting}
tuple(['cat', 'dog', 5]) # ('cat', 'dog', 5) 
list(('cat', 'dog', 5))  # ['cat', 'dog', 5] 
list('hello') # ['h', 'e', 'l', 'l', 'o']
\end{lstlisting}

\subsection{References}
As you’ve seen, variables store strings and integer values. However, this explanation is a simplification of what Python is actually doing. Technically, variables are storing references to the computer memory locations where the values are stored.


\paragraph{Identity}
We can use Python’s id() function to understand this. All values in Python have a unique identity that can be obtained with the id() function.

\begin{lstlisting}
id('Howdy') # 3341234
\end{lstlisting}

When Python runs id('Howdy'), it creates the 'Howdy' string in the computers memory. The numeric memory address where the string is stored is returned by the id() function. Python picks this address based on which memory bytes happen to be free on your computer at the time, so it’ll be different each time you run this code. Like all strings, Howdy is immutable and cannot be changed. If you change the string in a variable, a new string object is being made at a different place in memory, and the variable refers to this new string.

Pythons automatic garbage collector deletes any values not being referred to by any variables to free up memory. You don’t need to worry about how the garbage collector works, which is a good thing: manual memory management in other programming languages is a common source of bugs.

\paragraph{Passing References}
References are particularly important for understanding how arguments get passed to functions. When a function is called, the values of the arguments are copied to the parameter variables. For lists this means a copy of the reference is used for the parameter.

\begin{lstlisting}
def eggs(someParamter):
	someParameter.append('Hello')
	
spam = [1, 2, 3]
eggs(spam)
print(spam) # Output: [1, 2, 3, 'Hello']
\end{lstlisting}

Notice that when eggs() is called, a return value is not used to assign a new value to spam. Instead, it modifies the list in place, directly. Even though spam and someParameter contain separate references, they both refer to the same list. This is why the append('Hello') method call inside the function affects the list even after the function call has returned.

\paragraph{Copy Module}
Although passing around references is often the handiest way to deal with lists and dictionaries, if the function modifies the list or dictionary that is passed, you may not want these changes in the original list or dictionary value. For this, Python provides a module named copy that provides both the copy() and deepcopy() functions. The first of these, copy.copy(), can be used to make a duplicate copy of a mutable value like a list or dictionary, not just a copy of a reference.

\begin{lstlisting}
import copy
spam = ['A', 'B', 'C', 'D']
id(spam) # 12341234
cheese = copy.copy(spam)
id(cheese) # 12345678
\end{lstlisting}

If the list you need to copy contains lists, then use the copy.deepcopy() function instead of copy.copy(). The deepcopy() function will copy these inner lists as well.


