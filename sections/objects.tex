\section{Object-Oriented Programming}

\begin{description}
	\item [Encapsulation] The irst deinition is that encapsulation is the bundling of related data and code into a single unit. To encapsulate means to box up. This is essentially what classes do: they combine related attributes and methods.
	\item [Polymorphism] Polymorphism allows objects of one type to be treated as objects of another type. For example, the len() function returns the length of the argument passed to it. You can pass a string to len() to see how many characters it has, but you can also pass a list or dictionary to len() to see how many items or key-value pairs it has, respectively. This form of polymorphism is called generic functions or parametric polymorphism, because it can handle objects of many different types.

\end{description}

\subsection{Object-Oriented Programming and Classes}
For small programs, OOP doesn’t add organization so much as it adds bureaucracy. Although some languages, such as Java, require you to organize all your code into classes, Python’s OOP features are optional. Programmers can take advantage of classes if they need them or ignore them if they don’t.

You’ve already used classes and objects in Python, even if you haven’t created classes yourself. Consider the datetime module, which contains a class named date. Objects of the datetime.date class (also simply called datetime.date objects or date objects) represent a specific date. Attributes are variables associated with objects. The call to datetime.date() creates a new date object, initialized with the arguments 1999, 10, 31 so the object represents the date October 31, 1999. We assign these arguments as the date class’s year, month, and day attributes, which all date objects have.

\subsection{Simple Class}
Let’s create a WizCoin class, which represents a number of coins in a fictional wizard currency. In this currency, the denominations are knuts, sickles (worth 29 knuts), and galleons (worth 17 sickles or 493 knuts). Keep in mind that the objects in the WizCoin class represent a quantity of coins, not an amount of money. For example, it will inform you that you’re holding five quarters and one dime rather than \$1.35.

\lstinputlisting{src/SimpleClass/wizcoin.py}

As a convention, module names (like wizcoin in our wizcoin.py file) are lowercase, whereas class names (like WizCoin) begin with an uppercase letter. Unfortunately, some classes in the Python Standard Library, such as date, don’t follow this convention.

\begin{lstlisting}
import wizcoin
purse = wizcoin.WizCoin(2, 5, 99)
\end{lstlisting}

\paragraph{Methods, \_\_init\_\_() and self} \hfill \\
As you saw in the previous section, we create objects by calling the class name as a function. This function is referred to as a constructor function (or constructor, or abbreviated as ctor, pronounced “see-tore”) because it con- structs a new object. We also say the constructor instantiates a new instance of the class.

Calling the constructor causes Python to create the new object and then run the init() method. Classes aren’t required to have an init() method, but they almost always do. The init() method is where you commonly set the initial values of attributes and only there!

\paragraph{Attributes} \hfill \\
Attributes are variables associated with an object. Every object has its own set of attributes.  You can access and set these attributes just like any variable. In languages such as C++ or Java, attributes can be marked as having private access, which means the compiler or interpreter only lets code inside the class’s methods access or modify the attributes of objects of that class. But in Python, this enforcement doesn’t exist. All attributes and methods are effectively public access: code outside of the class can access and modify any attribute in any object of that class.

Python’s convention is to start private attribute or method names with a single underscore. Technically, there is nothing to stop code outside the class from accessing private attributes and methods, but it’s a best practice to let only the class’s methods access them.

\subsection{Object-Oriented Programming and Inheritance}
To create a new child class, you put the name of the existing parent class in between parentheses in the class statement.

\lstinputlisting{src/Inheritance/main.py}

We have created three classes named ParentClass, ChildClass and GrandChildClass. The ChildClass subclasses ParentClass, meaning that ChildClass will have all the same methods as ParentClass. We say that ChildClass inherits methods from ParentClass. Also, GrandchildClass subclasses ChildClass, so it has all the same methods as ChildClass and its parent, ParentClass. Any changes we make to the code in printHello() update not only ParentClass, but also ChildClass and GrandchildClass. This is the same as changing the code in a function updates all of its function calls.

\begin{figure}[h!]
  \center
  \includegraphics[width=0.75\textwidth]{inheritance}
  \caption{Inheritance}
\end{figure}

\paragraph{Overriding Methods} 
Child classes inherit all the methods of their parent classes. But a child
class can override an inherited method by providing its own method with
its own code. The child class’s overriding method will have the same name as the parent class’s method.

\paragraph{The super() function}
A child class’s overridden method is often similar to the parent class’s method. Even though inheritance is a code reuse technique, overriding a method might cause you to rewrite the same code from the parent class’s method as part of the child class’s method. To prevent this duplicate code, the built-in super() function allows an overriding method to call the original method in the parent class.

\subsubsection{Class Methods}
Class methods are associated with a class rather than with individual objects, like regular methods are. You can recognize a class method in code when you see two markers: the @classmethod decorator before the method’s def statement and the use of cls as the first parameter.

The cls parameter acts like self except self refers to an object, but the cls parameter refers to an object’s class. This means that the code in a class method cannot access an individual object’s attributes or call an object’s regular methods. Class methods can only call other class methods or access class attributes. We use the name cls because class is a Python keyword, and just like other keywords, such as if, while, or import, we can’t use it for parameter names. 

\begin{lstlisting}
class ExampleClass:
	def exampleRegularMethod(self):
		print('This is a regular mehtod')
	
	@classmethod
	def exampleClassMethod(cls):
		print('This is a class method')

ExampleClass.exampleClassMethod()
\end{lstlisting}

\subsubsection{Static Methods}
A static method doesn’t have a self or cls parameter. Static methods are effectively just functions, because they can’t access the attributes or methods of the class or its objects. Rarely, if ever, do you need to use static methods in Python. If you do decide to use one, you should strongly consider just creating a regular function instead. We deine static methods by placing the @staticmethod decorator before their def statements.

\begin{lstlisting}
class ExampleClassWithStaticMethod:     
	@staticmethod     
	def sayHello():         
		print('Hello!')
\end{lstlisting}

\subsubsection{Multiple Inheritance}
Python supports multiple parent classes by offering a feature called multiple inheritance. For example, we can have an Airplane class with a flyInTheAir() method and a Ship class with a floatOnWater() method. We could then create a FlyingBoat class that inherits from both Airplane and Ship by listing both in the class statement, separated by commas.

\lstinputlisting{src/Inheritance/multi.py}

Multiple inheritance is straightforward as long as the parent classes’ method names are distinct and don’t overlap. These sorts of classes are called mixins.

\subsubsection{Method Resolution Order}
For single inheritance, determining the MRO is easy: just make a chain of parent classes. For multiple inheritance, it’s trickier. Python’s MRO follows the C3 algorithm, whose details are beyond the scope. But you can determine the MRO by remembering two rules:

\begin{itemize}
	\item Python checks child classes before parent classes.
	\item Python checks inherited classes listed left to right in the class statement.
\end{itemize}


\subsection{Pyhtonic OOP}