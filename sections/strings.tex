\section{Manipulation Strings}
Text is one of the most common forms of data your programs will handle. You already know how to concatenate two string values together with the + operator, but youcan do much more than that. You can extract partial strings from string values, add or remove spacing, con- vert letters to lowercase or uppercase, and check that strings are formatted correctly. You can even write Python code to access the clipboard for copying and pasting text. 

\subsection{Working with Strings}
Typing string values in Python code is fairly straightforward: they begin and end with a single quote. But then how can you use a quote inside a string? Typing  'That is Alice's cat.' won’t work, because Python thinks the string ends after Alice, and the rest (s cat.') is invalid Python code. Fortunately, there are multiple ways to type strings.

\paragraph{Double Quotes} 
Strings can begin and end with double quotes, just as they do with single quotes. One benefit of using double quotes is that the string can have a single quote character in it. Since the string begins with a double quote, Python knows that the single quote is part of the string and not marking the end of the string. However, if you need to use both single quotes and double quotes in the string, you’ll need to use escape characters.


\begin{lstlisting}
spam = "That is Alice's cat"
\end{lstlisting}

\paragraph{Escape Characters}
An escape character lets you use characters that are otherwise impossible to put into a string. An escape character consists of a backslash (\textbackslash) followed by the character you want to add to the string.

\begin{figure}[h!]
  \center
  \includegraphics[width=0.5\textwidth]{escapechar}
  \caption{Escape Characters}
\end{figure}

\paragraph{Raw Strings}
You can place an r before the beginning quotation mark of a string to make it a raw string. A raw string completely ignores all escape characters and prints any backslash that appears in the string.

\begin{lstlisting}
print(r'That is Carol\'s cat.')
\end{lstlisting}

Because this is a raw string, Python considers the backslash as part of the string and not as the start of an escape character. Raw strings are helpful if you are typing string values that contain many backslashes, such as the strings used for Windows file paths like r'C:\textbackslash Users\textbackslash Al\textbackslash Desktop' or regular expressions.

\paragraph{Multiline Strings with Triple Quotes}
While you can use the \textbackslash n escape character to put a newline into a string, it is often easier to use multiline strings. A multiline string in Python begins and ends with either three single quotes or three double quotes. Any quotes, tabs, or newlines in between the “triple quotes” are considered part of the string.ß

\begin{lstlisting}
print('''Dear Alice,
	My cat's name is Ivan
Something''')
\end{lstlisting}

\paragraph{Multiline Comments}
While the hash character (\#) marks the beginning of a comment for the rest of the line, a multiline string is often used for comments that span multiple lines.

\begin{lstlisting}
"""This is a test Python programm
Written by Marco Agostini marco@edu.nothing
"""
\end{lstlisting}

\paragraph{Indexing and Slicing Strings}
Strings use indexes and slices the same way lists do. You can think of the string 'Hello, world!' as a list and each character in the string as an item with a corresponding index.

\begin{lstlisting}
spam = 'Hello, World!'
spam[0] # 'H'
spam[-1] # 'o'
\end{lstlisting}

If you specify an index, you’ll get the character at that position in the string. If you specify a range from one index to another, the starting index is included and the ending index is not. That’s why, if spam is 'Hello, world!', spam[0:5] is 'Hello'.

\paragraph{The in and not in Operators with Strings}
The in and not in operators can be used with strings just like with list values. An expression with two strings joined using in or not in will evaluate to a Boolean True or False.

\begin{lstlisting}
'Hello' in 'Hello, World' # True
'HELLO' in 'Hello, Wolrd' # False
\end{lstlisting}

\subsection{Putting Strings Inside Other Strings}
Putting strings inside other strings is a common operation in programming. So far, we’ve been using the + operator and string concatenation. However, this requires a lot of tedious typing. A simpler approach is to use string interpolation, in which the \%s operator inside the string acts as a marker to be replaced by values following the string. One benefit of string interpolation is that str() doesn’t have to be called to convert values to strings.

\begin{lstlisting}
name = 'Al'
age = 4000
'My name is %s, I am %s years old.' % (name, age)
\end{lstlisting}

\paragraph{f-strings}
Python 3.6 introduced f-strings, which is similar to string interpolation except that braces are used instead of \%s, with the expressions placed directly inside the braces.

\begin{lstlisting}
f'My name is {name}. Next year i will be {age + 1}.'
\end{lstlisting}

\subsection{Useful String Methods}
Several string methods analyze strings or create transformed string values. 

\paragraph{the upper(), lower(), isupper(), and islower() Mehtods}
The upper() and lower() string methods return a new string where all the letters in the original string have been converted to uppercase or lowercase, respectively. Nonletter characters in the string remain unchanged.

\begin{lstlisting}
spam = 'Hello, world!'
spam = spam.upper() # 'HELLO, WORLD!'
spam = spam.lower() # 'hello, world!'
\end{lstlisting}

Note that these methods do not change the string itself but return new string values. If you want to change the original string, you have to call upper() or lower() on the string and then assign the new string to the variable where the original was stored. This is why you must use spam = spam.upper() to change the string in spam instead of simply spam.upper(). 

The isupper() and islower() methods will return a Boolean True value if the string has at least one letter and all the letters are uppercase or lowercase, respectively. Otherwise, the method returns False.

\paragraph{The isX() Methods}
Along with islower() and isupper(), there are several other string methods that have names beginning with the word is. These methods return a Boolean value that describes the nature of the string. Here are some common isX string methods:

\begin{itemize}
	\item \lstinline|isalpha()| Returns True if the string consists only of letters and isn’t blank
	\item \lstinline|isalnum()|  Returns True if the string consists only of letters and numbers and is not blank
	\item \lstinline|isdecimal()|  Returns True if the string consists only of numeric characters and is not blank
	\item \lstinline|isspace()| Returns True if the string consists only of spaces, tabs, and newlines and is not blank
	\item \lstinline|istitle()| Returns True if the string consists only of words that begin with an uppercase letter followed by only lowercase letters
\end{itemize}


\paragraph{The startswith() and endswith() Methods}
The startswith() and endswith() methods return True if the string value they are called on begins or ends (respectively) with the string passed to the method; otherwise, they return False.

\paragraph{The join() and split() Methods}
The join() method is useful when you have a list of strings that need to be joined together into a single string value. The join() method is called on a string, gets passed a list of strings, and returns a string. The returned string is the concatenation of each string in the passed-in list.

\begin{lstlisting}
' '.join(['My', 'name', 'is', 'Simon'])
# 'My name is Simon'
'ABC'.join(['My', 'name', 'is', 'Simon']) 
# 'MyABCnameABCisABCSimon'
'My name is Simon'.split() 
# ['My', 'name', 'is', 'Simon']
\end{lstlisting}

\paragraph{Splitting String with the partition() Method}
The partition() string method can split a string into the text before and after a separator string. This method searches the string it is called on for the separator string it is passed, and returns a tuple of three substrings for the “before,” “separator,” and “after” substrings.

\begin{lstlisting}
'Hello, world!'.partition('w') 
# ('Hello, ', 'w', 'orld!') 
before, sep, after = 'Hello, world!'.partition(' ') 
\end{lstlisting}

\subsection{Numeric Values of Characters}
Every text character has a corresponding numeric value called a Unicode code point. For example, the numeric code point is 65 for 'A', 52 for '4', and 33 for '!'. You can use the ord() function to get the code point of a one-character string, and the chr() function to get the one-character string of an integer code point.

\begin{lstlisting}
ord('A') # = 65
ord('A') < ord('B') # True
\end{lstlisting}
